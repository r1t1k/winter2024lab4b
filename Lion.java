public class Lion{
    private String name;
    private double age;
    private int numOfLegs;
	public Lion(String name, double age, int numOfLegs){
		this.name = name;
		this.age = age;
		this.numOfLegs = numOfLegs;
	}
    public void sleep(){
        System.out.println(this.name + " is sleeping on his " + this.numOfLegs + " legs.");
    }

    public void age(){
        System.out.println(this.name + " is " + this.age + " years old.");
    }
	public String getName(){
		return this.name;
	}
	public double getAge(){
		return this.age;
	}
	public int getNumOfLegs(){
		return this.numOfLegs;
	}
	public void setNumOfLegs(int numOfLegs){
		this.numOfLegs = numOfLegs;
	}
}