import java.util.Scanner;
public class VirtualPetApp{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);

        Lion[] packOfLions = new Lion[1];

        for(int i = 0; i < packOfLions.length; i++){
            System.out.println("");
            System.out.println("What will be the name of your lion? ");
            String name = reader.nextLine();
            System.out.println("What is his age? ");
            double age = Double.parseDouble(reader.nextLine());
            System.out.println("How many legs does he have? ");
            int numOfLegs = Integer.parseInt(reader.nextLine());
			packOfLions[i] = new Lion(name, age, numOfLegs);
        }
		System.out.println(packOfLions[packOfLions.length-1].getName());
		System.out.println(packOfLions[packOfLions.length-1].getAge());
		System.out.println(packOfLions[packOfLions.length-1].getNumOfLegs());
		
		System.out.println("Give another amount of legs for your lion: ");
		packOfLions[packOfLions.length-1].setNumOfLegs(Integer.parseInt(reader.nextLine()));
		
		System.out.println(packOfLions[packOfLions.length-1].getName());
		System.out.println(packOfLions[packOfLions.length-1].getAge());
		System.out.println(packOfLions[packOfLions.length-1].getNumOfLegs());
    }
}